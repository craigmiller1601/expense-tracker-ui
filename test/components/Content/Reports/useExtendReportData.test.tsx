import { expect, test } from 'vitest';
import type { ReportPageResponse } from '../../../../src/types/generated/expense-tracker';
import reportResponseForExtension from '../../../__data__/report_response_for_extension.json';
import { useExtendReportData } from '../../../../src/components/Content/Reports/useExtendReportData';
import { renderHook } from '@testing-library/react';
import type { ExtendedReportMonthResponse } from '../../../../src/types/reports';
import { match } from 'ts-pattern';

const data: ReportPageResponse = reportResponseForExtension;

const expectedResultsNoNextRecord: ReadonlyArray<ExtendedReportMonthResponse> =
    [
        {
            ...reportResponseForExtension.reports[0],
            totalChange: 15,
            categories: [
                {
                    ...reportResponseForExtension.reports[0].categories[0],
                    amountChange: 2
                },
                {
                    ...reportResponseForExtension.reports[0].categories[1],
                    amountChange: -15
                },
                {
                    ...reportResponseForExtension.reports[0].categories[2],
                    amountChange: 4
                }
            ]
        },
        {
            ...reportResponseForExtension.reports[1],
            totalChange: -15,
            categories: [
                {
                    ...reportResponseForExtension.reports[1].categories[0],
                    amountChange: -2
                },
                {
                    ...reportResponseForExtension.reports[1].categories[1],
                    amountChange: -30
                },
                {
                    ...reportResponseForExtension.reports[1].categories[2],
                    amountChange: -4
                }
            ]
        },
        {
            ...reportResponseForExtension.reports[2],
            totalChange: undefined,
            categories: [
                {
                    ...reportResponseForExtension.reports[2].categories[0],
                    amountChange: undefined
                },
                {
                    ...reportResponseForExtension.reports[2].categories[1],
                    amountChange: undefined
                },
                {
                    ...reportResponseForExtension.reports[2].categories[2],
                    amountChange: undefined
                }
            ]
        }
    ];

test('extends report data for $date with no next record', () => {
    const { result } = renderHook(() => useExtendReportData(data));

    expect(result.current?.reports).not.toBeUndefined();
    expect(result.current?.reports).toEqual(expectedResultsNoNextRecord);
});

test('extends report data with next record', () => {
    const dataWithNextRecord: ReportPageResponse = {
        ...data,
        nextRecord: {
            date: '2023-10-01',
            total: -61,
            categories: [
                {
                    name: 'One',
                    amount: -23,
                    color: 'red',
                    percent: 0,
                    id: '1'
                },
                {
                    name: 'Two',
                    amount: -8,
                    color: 'red',
                    percent: 0,
                    id: '1'
                },
                {
                    name: 'Three',
                    amount: -30,
                    color: 'red',
                    percent: 0,
                    id: '1'
                }
            ]
        }
    };
    const { result } = renderHook(() =>
        useExtendReportData(dataWithNextRecord)
    );

    expect(result.current?.reports).not.toBeUndefined();
    expect(result.current?.reports).toEqual([
        ...expectedResultsNoNextRecord.slice(0, 2),
        {
            ...expectedResultsNoNextRecord[2],
            totalChange: 16,
            categories: expectedResultsNoNextRecord[2].categories.map(
                (category, index) => ({
                    ...category,
                    amountChange: match(index)
                        .with(0, () => 13)
                        .with(1, () => -7)
                        .with(2, () => 10)
                        .run()
                })
            )
        }
    ]);
});
