import { describe, expect, it } from 'vitest';
import { format, set } from 'date-fns/fp';
import {
    compareServerDates,
    DISPLAY_DATE_FORMAT,
    DISPLAY_DATE_TIME_FORMAT,
    formatDisplayDate,
    formatDisplayDateTime,
    formatReportMonth,
    formatServerDate,
    formatServerDateTime,
    parseDisplayDate,
    parseDisplayDateTime,
    parseServerDate,
    parseServerDateTime,
    REPORT_MONTH_FORMAT,
    SERVER_DATE_FORMAT,
    SERVER_DATE_TIME_FORMAT,
    serverDateTimeToDisplayDateTime,
    serverDateToDisplayDate,
    serverDateToReportMonth
} from '../../src/utils/dateTimeUtils';
import { SortDirection } from '../../src/types/misc';
import { formatInTimeZone, toZonedTime } from 'date-fns-tz';

describe('dateTimeUtils', () => {
    it('parseServerDate', () => {
        const date = new Date();
        const utc = toZonedTime(date, 'UTC');
        const expected = set({
            hours: 0,
            minutes: 0,
            seconds: 0,
            milliseconds: 0
        })(utc);
        const dateFormatted = formatInTimeZone(date, 'UTC', SERVER_DATE_FORMAT);
        const actual = parseServerDate(dateFormatted);
        expect(actual).toEqual(expected);
    });

    it('formatServerDate', () => {
        const date = new Date();
        const expected = format(SERVER_DATE_FORMAT)(date);
        expect(formatServerDate(date)).toBe(expected);
    });

    it('parseDisplayDate', () => {
        const date = new Date();
        const expected = set({
            hours: 0,
            minutes: 0,
            seconds: 0,
            milliseconds: 0
        })(date);
        const dateFormatted = format(DISPLAY_DATE_FORMAT)(date);
        const actual = parseDisplayDate(dateFormatted);
        expect(actual).toEqual(expected);
    });

    it('formatDisplayDate', () => {
        const date = new Date();
        const expected = format(DISPLAY_DATE_FORMAT)(date);
        const actual = formatDisplayDate(date);
        expect(actual).toEqual(expected);
    });

    it('parseServerDateTime', () => {
        const date = new Date();
        const serverDateTimeString = formatInTimeZone(
            date,
            'UTC',
            SERVER_DATE_TIME_FORMAT
        );
        const actual = parseServerDateTime(serverDateTimeString);
        expect(actual).toEqual(date);
    });

    it('parseServerDateTime with non-UTC timezome', () => {
        const date = new Date();
        const formatted = formatInTimeZone(
            date,
            'America/New_York',
            SERVER_DATE_TIME_FORMAT
        );
        const result = parseServerDateTime(formatted);
        expect(result).toEqual(date);
    });

    it('formatServerDateTime', () => {
        const date = new Date();
        const expected = formatInTimeZone(date, 'UTC', SERVER_DATE_TIME_FORMAT);
        const actual = formatServerDateTime(date);
        expect(actual).toEqual(expected);
    });

    it('parseDisplayDateTime', () => {
        const date = new Date();
        const expected = set({
            milliseconds: 0
        })(date);
        const formatted = format(DISPLAY_DATE_TIME_FORMAT)(date);
        const actual = parseDisplayDateTime(formatted);
        expect(actual).toEqual(expected);
    });

    it('formatDisplayDateTime', () => {
        const date = new Date();
        const expected = format(DISPLAY_DATE_TIME_FORMAT)(date);
        const actual = formatDisplayDateTime(date);
        expect(actual).toEqual(expected);
    });

    it('formatReportMonth', () => {
        const date = new Date();
        const expected = format(REPORT_MONTH_FORMAT)(date);
        const actual = formatReportMonth(date);
        expect(actual).toEqual(expected);
    });

    it('serverDateToDisplayDate', () => {
        const date = new Date();
        const formatted = format(SERVER_DATE_FORMAT)(date);
        const expected = format(DISPLAY_DATE_FORMAT)(date);
        const actual = serverDateToDisplayDate(formatted);
        expect(actual).toEqual(expected);
    });

    it('serverDateTimeToDisplayDateTime', () => {
        const date = new Date();
        const formatted = formatInTimeZone(
            date,
            'UTC',
            SERVER_DATE_TIME_FORMAT
        );
        const expected = format(DISPLAY_DATE_TIME_FORMAT)(date);
        const actual = serverDateTimeToDisplayDateTime(formatted);
        expect(actual).toEqual(expected);
    });

    it('serverDateToReportMonth', () => {
        const expected = 'Feb 2022';
        const actual = serverDateToReportMonth('2022-02-01');
        expect(actual).toEqual(expected);
    });

    it('compareServerDates', () => {
        const date1 = '2022-01-01';
        const date2 = '2022-02-01';

        expect(compareServerDates(date1, date2, SortDirection.ASC)).toBe(-1);
        expect(compareServerDates(date1, date1, SortDirection.ASC)).toBe(0);
        expect(compareServerDates(date2, date1, SortDirection.ASC)).toBe(1);

        expect(compareServerDates(date1, date2, SortDirection.DESC)).toBe(1);
        expect(compareServerDates(date1, date1, SortDirection.DESC)).toBe(0);
        expect(compareServerDates(date2, date1, SortDirection.DESC)).toBe(-1);
    });
});
