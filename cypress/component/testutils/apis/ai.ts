type Chainable<T> = Cypress.Chainable<T>;

const autoCategorizeUnconfirmed = (): Chainable<null> =>
    cy
        .intercept(
            'post',
            '/expense-tracker/api/ai/auto-categorize-unconfirmed',
            {
                fixture: 'empty.json'
            }
        )
        .as('autoCategorizeUnconfirmed');

export const aiApi = {
    autoCategorizeUnconfirmed
};
