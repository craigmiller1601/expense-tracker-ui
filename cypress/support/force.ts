export const forceClick = (subject: JQuery): Cypress.Chainable<JQuery> =>
    cy.wrap(subject).click({ force: true });

export const forceClear = (subject: JQuery): Cypress.Chainable<JQuery> =>
    cy.wrap(subject).clear({ force: true });

export const forceType = (subject: JQuery, text: string) =>
    cy.wrap(subject).type(text, {
        force: true
    });
