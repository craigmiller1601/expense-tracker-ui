import { namedLazy } from '../utils/reactWrappers';

export const Welcome = namedLazy(
    () => import('../components/Content/Welcome'),
    'Welcome'
);
export const Categories = namedLazy(
    () => import('../components/Content/Categories'),
    'Categories'
);
export const Import = namedLazy(
    () => import('../components/Content/Import'),
    'Import'
);
export const Transactions = namedLazy(
    () => import('../components/Content/Transactions'),
    'Transactions'
);
export const Rules = namedLazy(
    () => import('../components/Content/Rules'),
    'Rules'
);

export const Reports = namedLazy(
    () => import('../components/Content/Reports'),
    'Reports'
);
