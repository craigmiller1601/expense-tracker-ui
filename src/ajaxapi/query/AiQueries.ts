import {
    useMutation,
    type UseMutationResult,
    useQueryClient
} from '@tanstack/react-query';
import { aiAutoCategorizeUnconfirmed } from '../service/AiService';
import { SEARCH_FOR_TRANSACTIONS } from './TransactionQueries';

export const useAiAutoCategorizeTransactions = (): UseMutationResult<
    void,
    Error,
    void
> => {
    const queryClient = useQueryClient();
    return useMutation({
        mutationFn: () => aiAutoCategorizeUnconfirmed(),
        onSuccess: () =>
            queryClient.invalidateQueries({
                queryKey: [SEARCH_FOR_TRANSACTIONS]
            })
    });
};
