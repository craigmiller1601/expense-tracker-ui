import { expenseTrackerApi, getData } from './AjaxApi';

export const aiAutoCategorizeUnconfirmed = (): Promise<void> =>
    expenseTrackerApi
        .post<void, void>({
            uri: '/ai/auto-categorize-unconfirmed',
            errorCustomizer: 'Error using AI for auto-categorizing transactions'
        })
        .then(getData);
