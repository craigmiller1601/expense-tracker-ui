import { createContext, useContext } from 'react';

export type OnConfirmAction = () => void;
export type NewConfirmDialog = (
    title: string,
    message: string,
    onConfirmAction: OnConfirmAction
) => void;

export interface ConfirmDialogContextValue {
    readonly open: boolean;
    readonly title: string;
    readonly message: string;
    readonly onConfirmAction: OnConfirmAction;
    readonly onClose: () => void;
    readonly newConfirmDialog: NewConfirmDialog;
}

export const ConfirmDialogContext = createContext<ConfirmDialogContextValue>({
    open: false,
    title: '',
    message: '',
    onConfirmAction: () => {},
    onClose: () => {},
    newConfirmDialog: () => {}
});

export const useNewConfirmDialog = (): NewConfirmDialog =>
    useContext(ConfirmDialogContext).newConfirmDialog;
