import { useImmer } from 'use-immer';
import type { PropsWithChildren } from 'react';
import {
    ConfirmDialogContext,
    type ConfirmDialogContextValue,
    type OnConfirmAction
} from './ConfirmDialogContext';

interface State {
    readonly open: boolean;
    readonly title: string;
    readonly message: string;
    readonly onConfirmAction: OnConfirmAction;
}

export const ConfirmDialogProvider = (props: PropsWithChildren) => {
    const [state, setState] = useImmer<State>({
        open: false,
        title: '',
        message: '',
        onConfirmAction: () => {}
    });

    const contextValue: ConfirmDialogContextValue = {
        ...state,
        onClose: () =>
            setState((draft) => {
                draft.open = false;
            }),
        newConfirmDialog: (title, message, onConfirmAction) =>
            setState((draft) => {
                draft.open = true;
                draft.title = title;
                draft.message = message;
                draft.onConfirmAction = onConfirmAction;
            })
    };

    return (
        <ConfirmDialogContext.Provider value={contextValue}>
            {props.children}
        </ConfirmDialogContext.Provider>
    );
};
