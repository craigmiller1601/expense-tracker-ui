import type {
    ExtendedReportCategoryResponse,
    ReportCategoryOrderBy
} from '../../../types/reports';
import { match } from 'ts-pattern';
import * as RArray from 'fp-ts/ReadonlyArray';
import type { Ord } from 'fp-ts/Ord';
import type { ReportCategoryResponse } from '../../../types/generated/expense-tracker';

const sortByCategory: Ord<ReportCategoryResponse> = {
    equals: (a, b) => a.name === b.name,
    compare: (a, b) => {
        const result = a.name.localeCompare(b.name);
        if (result < 0) {
            return -1;
        } else if (result > 0) {
            return 1;
        }
        return 0;
    }
};

const sortByAmount: Ord<ReportCategoryResponse> = {
    equals: (a, b) => a.amount === b.amount,
    compare: (a, b) => {
        if (a.amount < b.amount) {
            return -1;
        } else if (a.amount > b.amount) {
            return 1;
        }
        return 0;
    }
};

export const sortCategories = (
    order: ReportCategoryOrderBy
): ((
    c: ReadonlyArray<ExtendedReportCategoryResponse>
) => ReadonlyArray<ExtendedReportCategoryResponse>) => {
    const sortBy = match(order)
        .with('AMOUNT', () => sortByAmount)
        .otherwise(() => sortByCategory);
    return RArray.sort(sortBy);
};
