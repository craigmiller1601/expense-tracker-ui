import { useIsAtLeastBreakpoint } from '../../../utils/breakpointHooks';
import type { Props } from './TransactionTable';

export const useIsEditMode = () => {
    const isAtLeastSm = useIsAtLeastBreakpoint('sm');
    return process.env.NODE_ENV === 'test' || isAtLeastSm;
};

export const arePropsEqual = (prevProps: Props, nextProps: Props): boolean => {
    const nextPropsEntries = Object.entries(nextProps) as ReadonlyArray<
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        [keyof Props, any]
    >;

    return (
        nextPropsEntries.filter(([key, value]) => {
            if (typeof value === 'function') {
                return false;
            }

            return value !== prevProps[key];
        }).length === 0
    );
};
